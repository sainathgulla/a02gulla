# README #

This README would normally document whatever steps are necessary to get your application up and running.

### A02 - JS (Personal Website Part 2)

### Select one option ###

* Option A - for those without prior programming experience
* Option B - for those with prior programming experience.
* Option C - for those who want to use the assignment to deploy a small client-side web app (must have some way to host it, e.g. free GitHub pages.) [1]

### Process ###

* Create a repository called A02Lastname. 
* In your A02Lastname folder, create a new starter project that uses BootStrap (you may use Initializr to begin).
* Meet with your group. Share your previous projects. Discuss what you are planning for your calculations.  What will you be able to implement by the time the assignment is due? 
* With your group, discuss the requirements for the assignment. Discuss which parts you might find challenging. Is there someone in the group who might be a good resource? Do you need to seek additional clarification regarding the requirements? What is your background?  

* What Option should you choose? 
    * Option A.  Your custom calculation must have at least 2 user inputs and display at least one calculated value.
    * Option B.  You meet all requirements of A and your custom calculation provides some type of useful application, includes more complex forms (e.g., with drop-downs, radio buttons, etc.). You use more than four user input values, perform more advanced functions, and display more extensive results than a single calculated value. 
    * Option C.  You will create a small custom application that meets all of the Option B requirements and you deploy it publicly (so we can hit your URL and test your site.

### Submission ###

* Your repo must be named correctly.
* Your site must use BootStrap and be responsive. Test it on phone-sized screens, a laptop, and a larger screen, if possible.
* Your menu must display your site name in the upper left. (The display does not have to be A02Lastname).
* Your menu must include a top-level menu item for your About page. Your About page should market you - your skills, education, interests, and provide contact information. You may use your previous work. 
* Your menu must display a top-level menu item for your Contact page. Your Contact page should allow the user to enter information to later email you. Just the form is needed - it does not need to actually send you an email. You may use your previous work.
* Your calculation page must meet the requirements for the Option you select. You must use JavaScript to modify some html element in the DOM (generally, to display the answer). You must use JQuery to modify some part of your site (e.g. HTML or CSS). Include a simple README.md file in your repository.
* Your work must be creative, unique, and your own work. You may discuss with your previous team members as well as your current team members. But each person must submit their own, unique version of their personal website. . 
* Test each other's websites with different inputs before finalizing your submissions. Try entering a string where a number is expected. Make sure it works (or provides a useful error). 
* Following the earlier example, create at least one QUnit test that tests at least one of your calculation methods. Test it with at least four different types of inputs (e.g. both positive numbers, a positive and negative number, with missing arguments (throw an error), with two negative numbers, with two zeros, etc.)
* Title your post "NN Lastname, Firstname - success" if successful, title your post "Lastname, Firstname" if you have questions. 
* List your team members (and any other collaborators).
* List any other references or websites you used for ideas, images, content, etc. 
* State the submission option you selected as a full sentence with your reasoning (e.g. I selected Option B because ... ). 
* Display a screen shot of your application with user input values.
* Display a screen shot of your application displaying the result. 
* Display a screen shot showing your successful tests. 
* Display a screen shot of your repository overview (will display your Readme.md). 
* Paste the JavaScript code for your calculations in the body of your text. Include at least one statement that alters HTML using only JavaScript (e.g. to display a result).
* Paste your code that uses JQuery to modify something on your site (e.g., HTML or CSS). 
* Include a link to your repository (and URL if deployed) with your submission. (If private, be sure to add your instructor and assistants to the repo.)

### Notes ###

* All screen shots must be visible, embedded, and show enough of your desktop to prove it is your laptop. 
* As part of your unique submission, you agree that course instructors and assistants can ask you to demonstrate and explain your code during class and/or lab sessions. 

* [1] To learn how to set up a site on GitHub pages, see https://pages.github.com/
* Choose your username with care.  If possible, it's nice to get your actual name, for example, my github username is denisecase, so my GitHub pages user site is http://denisecase.github.io/   
* There's a short article available here: http://denisecase.github.io/2015/06/26/new-site/ - it may be somewhat outdated already.