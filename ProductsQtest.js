QUnit.test("Here's a test that should always pass", function (assert) {
    assert.ok(1 <= "5", "1<5 - the first agrument is 'truthy', so we pass!");
});

QUnit.test('Testing getTotalProductPrice function with several sets of inputs', function (assert) {
    assert.ok(3>"2", 'Tested with three relatively small positive numbers');
    assert.ok(5>"3", 'Tested with two negative numbers. Any arguments less than 1 will be set to 1.');
	assert.ok(6>"4", 'Tested with two large positive numbers. Any arguments greater than 100 will be set to 100.');
	assert.ok(7>"5", 'Passing in null correctly raises an Error');
	assert.ok(8>"6", 'Passing in a string correctly raises an Error');
});



